﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace QBE
{
    public static class QueryByExampleExtension
    {
        public static IEnumerable<TSource> QueryByExample<TSource>(this IEnumerable<TSource> source, TSource filtros)
        {
            var query = source.AsQueryable();

            if (filtros == null)
                return query;

            var properties = filtros.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);

            foreach (var property in properties)
            {
                var value = property.GetValue(filtros, null);

                if (value == null) continue;

                if (property.PropertyType.IsValueType && Activator.CreateInstance(property.PropertyType).Equals(value)) continue;

                if (property.PropertyType.IsValueType || property.PropertyType == typeof(string))
                {
                    //.Where(x=>x.Id == 1)
                    var entityType = typeof(TSource);
                    var parameter = Expression.Parameter(entityType, "x");

                    var whereLambda = Expression.Lambda<Func<TSource, bool>>(Expression.Equal(
                                                                                        Expression.Property(parameter, property.Name),
                                                                                        Expression.Constant(value)
                                                                             ), parameter);

                    query = query.AsQueryable().Where(whereLambda);
                }
                else if (property.PropertyType.IsClass)
                {
                    //.Where(x => x.Departamento.Id == 1)
                    var entityType = typeof(TSource);
                    var parameter = Expression.Parameter(entityType, property.Name);

                    var innerProperties = property.PropertyType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

                    foreach (var innerProperty in innerProperties)
                    {
                        var innerValue = innerProperty.GetValue(value, null);

                        if (innerValue == null) continue;

                        if (innerProperty.PropertyType.IsValueType && Activator.CreateInstance(innerProperty.PropertyType).Equals(innerValue)) continue;

                        if (innerProperty.PropertyType.IsValueType || innerProperty.PropertyType == typeof(string))
                        {
                            var innerEntityType = innerProperty.PropertyType;
                            var innerPropertyName = innerProperty.Name;
                            var innerParameter = Expression.Property(parameter, property.Name);

                            var whereLambda = Expression.Lambda<Func<TSource, bool>>(Expression.Equal(
                                                                                                Expression.Property(innerParameter, innerProperty.Name),
                                                                                                Expression.Constant(innerValue)
                                                                                     ), parameter);

                            query = query.AsQueryable().Where(whereLambda);
                        }
                    }
                }
                else if (property.PropertyType.IsGenericType)
                {
                    //filtro.Telefones.Where(x => x != null).Select(x => x.Id);
                    var filterGenericsType = property.PropertyType.GetGenericArguments().ElementAt(0);

                    var innerProperties = filterGenericsType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

                    var filters = ((IEnumerable)property.GetValue(filtros, null)).AsQueryable();

                    foreach (var innerProperty in innerProperties)
                    {

                        if (innerProperty.PropertyType.IsValueType || innerProperty.PropertyType == typeof(string))
                        {
                            var collectionPropertyType = innerProperty.PropertyType;
                            var collectionPropertyName = innerProperty.Name;

                            var filterParameter = Expression.Parameter(filterGenericsType, filterGenericsType.Name);

                            var whereNotNull = Expression.Lambda(Expression.NotEqual(filterParameter, Expression.Constant(null)),
                                                                 filterParameter);

                            var callWhereNotNull = Expression.Call(typeof(Enumerable),
                                                                   "Where",
                                                                   new Type[] { filterGenericsType },
                                                                   filters.Expression,
                                                                   whereNotNull);


                            var selectIds = Expression.Lambda(Expression.PropertyOrField(filterParameter, collectionPropertyName), filterParameter);

                            var callSelectIds = Expression.Call(typeof(Enumerable),
                                                                      "Select",
                                                                      new Type[] { filterGenericsType, collectionPropertyType },
                                                                      callWhereNotNull,
                                                                      selectIds);

                            var Ids = Expression.Lambda(callSelectIds).Compile().DynamicInvoke();

                            //.Where(p => p.Telefones.Select(t => t.Id).Any(id => Ids.Contains(id)));
                            var entityType = typeof(TSource);
                            var selectParameter = Expression.Parameter(entityType, filterGenericsType.Name);

                            var selectLambda = Expression.Lambda(Expression.Property(filterParameter, collectionPropertyName), filterParameter);
                            var selectCall = Expression.Call(typeof(Enumerable),
                                                     "Select",
                                                     new Type[] { filterGenericsType, collectionPropertyType },
                                                     Expression.Property(selectParameter, property.Name),
                                                     selectLambda);

                            var containsParameter = Expression.Parameter(collectionPropertyType, collectionPropertyName);
                            var containsCall = Expression.Call(typeof(Enumerable),
                                                  "Contains",
                                                  new Type[] { collectionPropertyType },
                                                  Expression.Constant(Ids),
                                                  containsParameter);

                            var anyLambda = Expression.Lambda(containsCall, containsParameter);

                            var anyCall = Expression.Call(typeof(Enumerable),
                                         "Any",
                                         new Type[] { collectionPropertyType },
                                         selectCall,
                                         anyLambda);

                            var whereLambda = Expression.Lambda<Func<TSource, bool>>(anyCall, selectParameter);

                            query = query.Where(whereLambda);
                        }
                    }
                }
            }

            return query;
        }
    }
}
