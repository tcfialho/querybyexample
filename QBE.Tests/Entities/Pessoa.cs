﻿using System.Collections.Generic;

namespace QBE.Tests.Entities
{
    public class Pessoa
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public virtual Departamento Departamento { get; set; }
        public virtual IList<Telefone> Telefones { get; set; }
    }
}
