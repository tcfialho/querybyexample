﻿namespace QBE.Tests.Entities
{
    public class Telefone
    {
        public int Id { get; set; }
        public string Numero { get; set; }
    }
}
