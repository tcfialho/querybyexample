﻿using QBE.Tests.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QBE.Tests
{
    public class Context : DbContext
    {
        public DbSet<Pessoa> Pessoas { get; set; }
        public DbSet<Departamento> Departamentos { get; set; }
        public DbSet<Telefone> Telefones { get; set; }

        public Context() : base()
        {
            this.Configuration.AutoDetectChangesEnabled = true;
            this.Configuration.LazyLoadingEnabled = true;
            this.Configuration.ProxyCreationEnabled = true;
            this.Configuration.ValidateOnSaveEnabled = false;

            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);

            Database.SetInitializer(new ContextInitializer());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }

    public class ContextInitializer : DropCreateDatabaseAlways<Context>
    {
        public static IEnumerable<Telefone> Telefones { get; } = GetTelefones();
        public static IEnumerable<Departamento> Departamentos { get; } = GetDepartamentos();
        public static IEnumerable<Pessoa> Pessoas { get; } = GetPessoas();

        public static IEnumerable<Pessoa> GetPessoas()
        {
            IList<Pessoa> pessoas = new List<Pessoa>();
            pessoas.Add(new Pessoa() { Id = 1, Nome = "Thiago", Departamento = Departamentos.First(), Telefones = Telefones.Take(2).ToList() });
            pessoas.Add(new Pessoa() { Id = 2, Nome = "Pedro", Departamento = Departamentos.Last(), Telefones = Telefones.Skip(2).Take(2).ToList() });
            return pessoas;
        }

        public static IEnumerable<Departamento> GetDepartamentos()
        {
            IList<Departamento> departamentos = new List<Departamento>();
            departamentos.Add(new Departamento() { Id = 1, Nome = "TI" });
            departamentos.Add(new Departamento() { Id = 2, Nome = "RH" });
            return departamentos;
        }

        public static IEnumerable<Telefone> GetTelefones()
        {
            IList<Telefone> telefones = new List<Telefone>();
            telefones.Add(new Telefone() { Id = 1, Numero = "1199999999" });
            telefones.Add(new Telefone() { Id = 2, Numero = "1188888888" });
            telefones.Add(new Telefone() { Id = 3, Numero = "1177777777" });
            telefones.Add(new Telefone() { Id = 4, Numero = "1166666666" });
            return telefones;
        }

        protected override void Seed(Context context)
        {
            context.Departamentos.AddRange(Departamentos);
            context.Telefones.AddRange(Telefones);
            context.Pessoas.AddRange(Pessoas);

            base.Seed(context);
        }
    }

}
