﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using QBE.Tests.Entities;

namespace QBE.Tests
{
    [TestClass]
    public class ShouldQueryCollection
    {
        #region stub

        public static IEnumerable<Telefone> Telefones { get; } = GetTelefones();
        public static IEnumerable<Departamento> Departamentos { get; } = GetDepartamentos();
        public static IEnumerable<Pessoa> Pessoas { get; } = GetPessoas();

        public static IEnumerable<Pessoa> GetPessoas()
        {
            IList<Pessoa> pessoas = new List<Pessoa>();
            pessoas.Add(new Pessoa() { Id = 1, Nome = "Thiago", Departamento = Departamentos.First(), Telefones = Telefones.Take(2).ToList() });
            pessoas.Add(new Pessoa() { Id = 2, Nome = "Pedro", Departamento = Departamentos.Last(), Telefones = Telefones.Skip(2).Take(2).ToList() });
            return pessoas;
        }

        public static IEnumerable<Departamento> GetDepartamentos()
        {
            IList<Departamento> departamentos = new List<Departamento>();
            departamentos.Add(new Departamento() { Id = 1, Nome = "TI" });
            departamentos.Add(new Departamento() { Id = 2, Nome = "RH" });
            return departamentos;
        }

        public static IEnumerable<Telefone> GetTelefones()
        {
            IList<Telefone> telefones = new List<Telefone>();
            telefones.Add(new Telefone() { Id = 1, Numero = "1199999999" });
            telefones.Add(new Telefone() { Id = 2, Numero = "1188888888" });
            telefones.Add(new Telefone() { Id = 3, Numero = "1177777777" });
            telefones.Add(new Telefone() { Id = 4, Numero = "1166666666" });
            return telefones;
        }

        #endregion

        [TestMethod]
        public void WhenIntegerPropertyIsFilled()
        {
            //Arrange
            var arrange = new Pessoa() { Id = 2 };

            //Act
            var act = Pessoas.QueryByExample(arrange);

            //Assert
            Assert.IsTrue(act.First().Id == arrange.Id);
        }

        [TestMethod]
        public void WhenStringPropertIsFilled()
        {
            //Arrange
            var arrange = new Pessoa() { Nome = "Thiago" };

            //Act
            var act = Pessoas.QueryByExample(arrange);

            //Assert
            Assert.IsTrue(act.First().Nome == arrange.Nome);
        }

        [TestMethod]
        public void WhenClassPropertyIsFilled()
        {
            //Arrange
            var arrange = new Pessoa() { Departamento = Departamentos.Last() };

            //Act
            var act = Pessoas.QueryByExample(arrange);

            //Assert
            Assert.IsTrue(act.First().Departamento.Equals(arrange.Departamento));
        }

        [TestMethod]
        public void WhenClassPropertyIntegerMemberIsFilled()
        {
            //Arrange
            var arrange = new Pessoa() { Departamento = new Departamento() { Id = 1 } };

            //Act
            var act = Pessoas.QueryByExample(arrange);

            //Assert
            Assert.IsTrue(act.First().Departamento.Id == arrange.Departamento.Id);
        }

        [TestMethod]
        public void WhenClassPropertyStringMemberIsFilled()
        {
            //Arrange
            var arrange = new Pessoa() { Departamento = new Departamento() { Nome = "RH" } };

            //Act
            var act = Pessoas.QueryByExample(arrange);

            //Assert
            Assert.IsTrue(act.First().Departamento.Nome == arrange.Departamento.Nome);
        }

        [TestMethod]
        public void WhenCollectionIsFilled()
        {
            //Arrange
            var arrange = new Pessoa() { Telefones = new Telefone[] { Telefones.Last(), Telefones.First() } };

            //Act
            var act = Pessoas.QueryByExample(arrange);

            //Assert
            Assert.IsTrue(act.First().Telefones.Any(x => x.Id == 1));
            Assert.IsTrue(act.Last().Telefones.Any(x => x.Id == 4));
        }
    }
}
