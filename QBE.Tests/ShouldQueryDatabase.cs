﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Linq;
using QBE.Tests.Entities;

namespace QBE.Tests
{
    [TestClass]
    public class ShouldQueryDatabase
    {
        [TestMethod]
        public void WhenIntegerPropertyIsFilled()
        {
            using (var context = new Context())
            {
                //Arrange                
                var arrange = new Pessoa() { Id = 2 };

                //Act
                var act = context.Pessoas.QueryByExample(arrange).FirstOrDefault();

                //Assert
                Assert.IsTrue(act.Id == arrange.Id);
            }
        }

        [TestMethod]
        public void WhenClassPropertyIntegerMemberIsFilled()
        {
            using (var context = new Context())
            {
                //Arrange                
                var arrange = new Pessoa() { Departamento = context.Departamentos.OrderByDescending(o => o.Id).First() };

                //Act
                var act = context.Pessoas.QueryByExample(arrange).FirstOrDefault();

                //Assert
                Assert.IsTrue(act.Departamento.Nome == arrange.Departamento.Nome);
            }
        }

        [TestMethod]
        public void WhenCollectionIsFilled()
        {
            using (var context = new Context())
            {
                //Arrange   
                var arrange = new Pessoa() { Telefones = new Telefone[] { context.Telefones.OrderByDescending(o => o.Id).First(), context.Telefones.First() } };

                //Act
                var act = context.Pessoas.QueryByExample(arrange).ToList();

                //Assert
                Assert.IsTrue(act.First().Telefones.Any(x => x.Id == 1));
                Assert.IsTrue(act.Last().Telefones.Any(x => x.Id == 4));
            }
        }

    }
}
